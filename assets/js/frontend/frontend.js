import Navbar from "./components/navbar";
import CategoriesList from "./components/categories-list";

function docReady(fn) {
	// see if DOM is already available
	if (document.readyState === "complete" || document.readyState === "interactive") {
		// call on next available tick
		setTimeout(fn, 1);
	} else {
		document.addEventListener("DOMContentLoaded", fn);
	}
}

docReady(function () {
	new Navbar();
	new CategoriesList();
});
