export default class CategoriesList {
	constructor() {
		this.sliders = document.querySelectorAll('.categories-list');
		this.mouseDown = false;
		this.startX = 0;
		this.scrollLeft = 0;

		this.events();
	}

	events() {
		this.sliders.forEach(slider => {
			slider.addEventListener('mousemove', this.handleDrag, false);
			slider.addEventListener('mousedown', this.handleStartDragging, false);
			slider.addEventListener('mouseup', this.handleStopDragging, false);
			slider.addEventListener('mouseleave', this.handleStopDragging, false);
		});
	}

	handleStartDragging(event) {
		this.mouseDown = true;
		this.startX = event.pageX - event.currentTarget.offsetLeft;
		this.scrollLeft = event.currentTarget.scrollLeft;
	}

	handleStopDragging() {
		this.mouseDown = false;
	}

	handleDrag(event) {
		event.preventDefault();

		if (!this.mouseDown) {
			return;
		}

		const x = event.pageX - event.currentTarget.offsetLeft;

		const scroll = x - this.startX;

		event.currentTarget.scrollLeft = (this.scrollLeft - scroll) / 2;
	}
}
