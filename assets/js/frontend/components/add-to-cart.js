export default class AddToCart {
	constructor() {
		this.productsAddToCartAjax = document.querySelectorAll('.product-ajax-add-to-cart');

		this.events();
	}

	events() {
		this.handleChangeQuantity();
	}

	handleChangeQuantity() {
		this.productsAddToCartAjax.forEach((product) => {
			const quantityInput = product.querySelector('input.qty');
			const addToCartButton = product.querySelector('.ajax_add_to_cart');

			// Check if both addToCartButton and quantityInput exist to avoid errors
			if (quantityInput && addToCartButton) {
				quantityInput.addEventListener('change', (event) => {
					addToCartButton.dataset.quantity = event.currentTarget.value;
				});

				// Observes the css class change on the add to cart button to reset the quantity field
				let prevClassState = addToCartButton.classList.contains('added');
				const addToCartButtonObserver = new MutationObserver((mutations) => {
					mutations.forEach((mutation) => {
						if (mutation.attributeName === "class") {
							let currentClassState = mutation.target.classList.contains('added');

							if (prevClassState !== currentClassState) {
								prevClassState = currentClassState;

								if (currentClassState) {
									quantityInput.value = 1;
									addToCartButton.dataset.quantity = 1;
								}

							}
						}
					});
				});

				addToCartButtonObserver.observe(addToCartButton, {
					attributes: true,
					attributeFilter: ['class'],
					childList: false,
					characterData: false
				});
			}

		});
	}
}
