export default class Quantity {
	constructor() {
		this.quantities = document.querySelectorAll('.quantity');
		this.updateCartButton = document.querySelector('[name="update_cart"]');

		this.events();
	}

	events() {
		// Triggers for various types of events
		["load"].forEach((event) => {
			window.addEventListener(event, () => {
				this.handleQuantityChange();
			});
		});

		/**
		 * Lister to WooCommerce cart update javascript custom event to bind our handler again.
		 *
		 * Unfortunately it has to be jQuery
		 *
		 * @see https://stackoverflow.com/questions/64138023/woocommerce-js-event-doesnt-work-using-vanilla-js-but-works-fine-with-jquery
		 */
		jQuery(document.body).on('updated_cart_totals', () => {
			// Needs to be reselected
			this.quantities = document.querySelectorAll('.quantity');
			this.updateCartButton = document.querySelector('[name="update_cart"]');

			this.handleQuantityChange();
		});
	}

	handleQuantityChange() {
		this.quantities.forEach((quantity) => {
			const quantityInput = quantity.querySelector('input.qty');
			const quantityMinusButton = quantity.querySelector('button[data-action="minus"]');
			const quantityPlusButton = quantity.querySelector('button[data-action="plus"]')

			const quantityInputChange = new Event('change');

			if (quantityInput && quantityMinusButton && quantityPlusButton) {
				quantityMinusButton.addEventListener('click', () => {
					const currentValue = parseInt(quantityInput.value, 10);

					const newValue = isNaN(currentValue) ? 1 : currentValue - 1;

					if (newValue >= quantityInput.min) {
						quantityInput.value = newValue;

						quantityInput.dispatchEvent(quantityInputChange);

						this.enableUpdateCartButton();
					}
				});

				quantityPlusButton.addEventListener('click', () => {
					const currentValue = parseInt(quantityInput.value, 10);

					const newValue = isNaN(currentValue) ? 1 : currentValue + 1;

					quantityInput.value = newValue;

					quantityInput.dispatchEvent(quantityInputChange);

					this.enableUpdateCartButton();
				});
			}
		});
	}

	enableUpdateCartButton() {
		if (this.updateCartButton) {
			this.updateCartButton.disabled = false;
			this.updateCartButton.setAttribute('aria-disabled', 'false');
		}
	}
}
