export default class Navbar {
	constructor() {
		this.body = document.querySelector("body");
		this.wpadminbar = document.querySelector("#wpadminbar");
		this.navbarMain = document.querySelector("#navbar");
		this.allNavbar = document.querySelectorAll(".navbar");
		this.viwewportX = document.documentElement.clientWidth;
		this.offset = this.navbarMain.offsetHeight;
		this.events();
	}

	// Events Triggers
	events() {
		if (this.navbarMain) {
			window.addEventListener("scroll", () => {
				this.handleStickEffect();
				this.handleWhenLoggedIn();
			});

			window.addEventListener("resize", () => {
				this.handleViwewportSize();
			});
		}

		if (this.allNavbar) {
			this.handleNavbarToggle();
		}
	}

	// Methods
	handleStickEffect() {
		if (window.scrollY > this.offset) {
			this.navbarMain.classList.add("sticky");
		} else {
			this.navbarMain.classList.remove("sticky");
		}
	}

	handleWhenLoggedIn() {
		// When the WP Admin Bar is visible, we need to change the top space to stick the navbar menu visible below it
		if (window.scrollY > this.offset) {
			if (this.wpadminbar && this.viwewportX > 576)
				this.navbarMain.style.top = this.wpadminbar.offsetHeight + "px";
		} else {
			if (this.wpadminbar && this.viwewportX > 576)
				this.navbarMain.style.top = "initial";
		}
	}

	handleViwewportSize() {
		this.viwewportX = document.documentElement.clientWidth;
	}

	handleNavbarToggle() {
		this.allNavbar.forEach((navbar) => {
			const navbarTogglers = navbar.querySelectorAll('.navbar-toggler');

			if (navbarTogglers) {

				navbarTogglers.forEach((navbarToggler) => {
					navbarToggler.addEventListener('click', (toggler) => {
						let target = toggler.currentTarget.dataset.target;
						let icon = toggler.currentTarget.querySelector(".navbar-toggler-icon");

						target = document.querySelector(target);

						if (target) {
							this.body.classList.toggle("no-scroll-vertical");

							navbarToggler.classList.toggle("show");
							target.classList.toggle("show");
						}

						if (icon) {
							icon.classList.toggle("close");
						}
					});
				});

			}
		});
	}
}
