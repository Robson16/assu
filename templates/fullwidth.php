<?php
/*
Template Name: Full-Width
Template Post Type: post
*/

get_header();
?>

<main>
	<?php
	while (have_posts()) {
		the_post();
		get_template_part('partials/content/content', 'fullwidth');
	}
	?>

	<div class="container-fluid">
		<?php
		if (comments_open() || get_comments_number()) comments_template();

		get_template_part('partials/content/content', 'related');
		?>
	</div>
	<!-- /.container-fluid -->
</main>

<?php
get_footer();
