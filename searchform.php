<?php

/**
 * Template for displaying search forms
 *
 * @link https://polylang.pro/always-use-get_search_form-to-create-search-forms/
 */
?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url(home_url('/')); ?>">
	<label class="search-label" for="s">
		<?php _e('Search for:', 'assu'); ?>
		<input type="search" id="s" name="s" class="search-field" placeholder="<?php _e('Search for', 'assu') ?>" value="<?php echo get_search_query(); ?>" aria-label="<?php _e('Search for', 'assu'); ?>" />
	</label>

	<button class="search-submit" type="submit">
		<?php esc_html_e('Search', 'assu'); ?>
	</button>
</form>
