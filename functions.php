<?php

/**
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

// Enqueue for Front-End
function assu_scripts()
{
	// CSS
	wp_enqueue_style('assu-shared-styles', get_template_directory_uri() . '/assets/css/shared/shared-styles.css', array(), wp_get_theme()->get('Version'));
	wp_enqueue_style('assu-frontend-styles', get_template_directory_uri() . '/assets/css/frontend/frontend-styles.css', array(), wp_get_theme()->get('Version'));

	// Js
	wp_enqueue_script('comment-reply');
	wp_enqueue_script('assu-frontend-script', get_template_directory_uri() . '/assets/js/frontend/frontend-bundled.js', array(), wp_get_theme()->get('Version'), true);
}
add_action('wp_enqueue_scripts', 'assu_scripts');

// Enqueue for Login Page
function assu_login_scripts()
{
	wp_enqueue_style('assu-login', get_template_directory_uri() . '/assets/css/admin/login-styles.css', array(), wp_get_theme()->get('Version'));
}
add_action('login_enqueue_scripts', 'assu_login_scripts');

/**
 * Set theme defaults and register support for various WordPress features.
 */
function assu_setup()
{
	// Enabling translation support
	$textdomain = 'assu';
	load_theme_textdomain($textdomain, get_stylesheet_directory() . '/languages/');
	load_theme_textdomain($textdomain, get_template_directory() . '/languages/');

	// Customizable logo
	add_theme_support('custom-logo', array(
		'height'      => 80,
		'width'       => 260,
		'flex-height' => false,
		'flex-width'  => false,
		'header-text' => array('site-title', 'site-description'),
	));

	// Custom Header
	add_theme_support('custom-header', array(
		'default-image'      => get_template_directory_uri() . '/assets/images/default-header.jpg',
		'default-text-color' => 'ffffff',
		'width'              => 1320,
		'height'             => 190,
		'flex-width'         => true,
		'flex-height'        => true,
	));


	// Menu registration
	register_nav_menus(array(
		'main_menu' => __('Main Menu', 'assu'),
		'mobile_menu' => __('Mobile Menu', 'assu'),
	));

	// Load custom styles in the editor.
	add_theme_support('editor-styles');
	add_editor_style(get_stylesheet_directory_uri() . '/assets/css/shared/shared-styles.css');
	add_editor_style(get_stylesheet_directory_uri() . '/assets/css/admin/editor-styles.css');

	// Let WordPress manage the document title.
	add_theme_support('title-tag');

	// Enable support for featured image on posts and pages.
	add_theme_support('post-thumbnails');

	// Enable support for embedded media for full weight
	add_theme_support('responsive-embeds');
}
add_action('after_setup_theme', 'assu_setup');

/**
 * Registration of widget areas.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function assu_sidebars()
{
	// Args used in all calls register_sidebar().
	$shared_args = array(
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
		'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
		'after_widget' => '</div></div>',
	);

	// Footer #1
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #1', 'assu'),
		'id' => 'assu-sidebar-footer-1',
		'description' => __('The widgets in this area will be displayed in the first column in the footer.', 'assu'),
	)));

	// Footer #2
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #2', 'assu'),
		'id' => 'assu-sidebar-footer-2',
		'description' => __('The widgets in this area will be displayed in the second column in the footer.', 'assu'),
	)));

	// Footer #3
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #3', 'assu'),
		'id' => 'assu-sidebar-footer-3',
		'description' => __('The widgets in this area will be displayed in the third column in the footer.', 'assu'),
	)));

	// Footer #4
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #4', 'assu'),
		'id' => 'assu-sidebar-footer-4',
		'description' => __('The widgets in this area will be displayed in the fourth column in the footer.', 'assu'),
	)));

	// Footer #5
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #5', 'assu'),
		'id' => 'assu-sidebar-footer-5',
		'description' => __('The widgets in this area will be displayed in the fifth column in the footer.', 'assu'),
	)));

	// Barra Lateral Blog #1
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Sidebar Blog', 'assu'),
		'id' => 'assu-sidebar-blog',
		'description' => __('The widgets in this area will be displayed in the blog sidebar.', 'assu'),
	)));
}
add_action('widgets_init', 'assu_sidebars');

/**
 * Remove website field from comment form
 *
 */
function assu_website_remove($fields)
{
	if (isset($fields['url']))
		unset($fields['url']);
	return $fields;
}
add_filter('comment_form_default_fields', 'assu_website_remove');

/**
 * Add a new avatar option in Settings » Discussion
 *
 * It only works well when in a 100% online environment, as WordPress sends the image to Gravatar to validate the image,
 * in a localhost environment, use another option to not have a broken image on the backend and frontend
 *
 */
function assu_default_avatar($avatar_defaults)
{
	$myavatar =	get_template_directory_uri() . '/assets/images/default-avatar.png';
	$avatar_defaults[$myavatar] = __('Assu Avatar', 'assu');
	return $avatar_defaults;
}
add_filter('avatar_defaults', 'assu_default_avatar');

// Change the login logo
function assu_login_logo()
{
	$logo = get_theme_mod('custom_logo');
	$image = wp_get_attachment_image_src($logo, 'full');
	$image_url = $image[0];

	echo '<style type="text/css">h1 a { background-image: url(' . $image_url . ')!important; }</style>';
}
add_action('login_head', 'assu_login_logo');

function assu_login_url($url)
{
	return esc_url(get_site_url());
}
add_filter('login_headerurl', 'assu_login_url');

/**
 *  TGM Plugin
 */
require_once get_template_directory() . '/includes/required-plugins.php';

/**
 *  WordPress Bootstrap Nav Walker
 */
require_once get_template_directory() . '/includes/classes/class-wp-bootstrap-navwalker.php';

/**
 * 	Custom menu item field
 */
require_once get_template_directory() . '/includes/custom-menu-item-field.php';

/**
 * Custom navbar login form
 */
require_once get_template_directory() . '/includes/custom-navbar-login-form.php';

/**
 *  WooCommerce Functions
 */
if (class_exists('WooCommerce')) {
	require_once get_template_directory() . '/includes/woocommerce-functions.php';
}
