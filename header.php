<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<header id="header">
		<nav id="navbar" class="navbar">
			<div class="container">
				<?php
				if (has_custom_logo()) {
					the_custom_logo();
				} else {
					echo '<h1 style="margin: 0;">' . get_bloginfo('title') . '</h1>';
				}
				?>

				<div class="navbar-main mobile">
					<?php if (is_user_logged_in()) : ?>
						<div class="logged">
							<?php
							echo wp_get_current_user()->display_name;
							echo get_avatar(wp_get_current_user()->ID, 37, '', wp_get_current_user()->display_name);
							?>
						</div>
					<?php else : ?>
						<?php if (class_exists('WooCommerce') && 'yes' === get_option('woocommerce_enable_myaccount_registration')) : ?>
							<a class="register-link" href="<?php echo esc_url(get_permalink(get_option('woocommerce_myaccount_page_id'))); ?>">
								<?php esc_html_e('Create an account', 'assu'); ?>
							</a>
						<?php elseif (get_option('users_can_register')) : ?>
							<a class="register-link" href="<?php echo esc_url(wp_registration_url()); ?>">
								<?php esc_html_e('Create an account', 'assu'); ?>
							</a>
						<?php endif; ?>
					<?php endif; ?>
				</div>
				<!-- /.navbar-main /.mobile -->

				<div class="navbar-main desktop">
					<button type="button" class="navbar-toggler" data-target="#navbar-nav">
						<span class="navbar-toggler-icon">
							<div class="bar1"></div>
							<div class="bar2"></div>
							<div class="bar3"></div>
						</span>
					</button>

					<?php
					wp_nav_menu(array(
						'theme_location' => 'main_menu',
						'depth' => 2,
						'container' => 'div',
						'container_class' => 'collapse navbar-collapse ',
						'container_id' => 'navbar-nav',
						'menu_class' => 'navbar-nav ',
						'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
						'walker' => new WP_Bootstrap_Navwalker()
					));
					?>
				</div>
				<!-- /.navbar-main /.desktop -->
			</div>
			<!-- /.container -->
		</nav>
		<!-- /.navbar -->

		<?php get_template_part('partials/navbar/navbar', 'mobile'); ?>
	</header>
