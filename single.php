<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 */
get_header();
?>

<main class="container">
	<section class="single-content">
		<?php
		while (have_posts()) {
			the_post();
			get_template_part("partials/content/content", get_post_format());
		}

		if (comments_open() || get_comments_number()) comments_template();
		?>
	</section>
	<!-- /.single-content -->

	<?php
	get_template_part("partials/content/content", "related");

	get_sidebar();
	?>
</main>
<!-- /.container -->

<?php
get_footer();
