<?php

/**
 * Filter to change the catalogue ordering options
 */

add_filter('woocommerce_catalog_orderby', 'assu_orderby_options', 20);

function assu_orderby_options($options)
{
	$options['menu_order'] 	= __('Default sorting', 'assu');
	$options['popularity'] 	= __('Best seller', 'assu');
	$options['rating']     	= __('Top Ratings', 'assu');
	$options['date']      	= __('Release date', 'assu');
	$options['price']     	= __('Lowest price', 'assu');
	$options['price-desc'] 	= __('Biggest price', 'assu');

	return $options;
}
