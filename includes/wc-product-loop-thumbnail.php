<?php

/**
 * Edit the attachment image attributes for a WooCommerce Product
 *
 * @param string[]     $attr       Array of attribute values for the image markup, keyed by attribute name.
 *                                 See wp_get_attachment_image().
 * @param WP_Post      $attachment Image attachment post.
 * @param string|int[] $size       Requested image size. Can be any registered image size name, or
 *                                 an array of width and height values in pixels (in that order).
 */

add_filter('wp_get_attachment_image_attributes', 'assu_attachment_image_attributes', 10, 3);
function assu_attachment_image_attributes($attr, $attachment, $size)
{
	// Is a WC product
	$post = wc_get_product($attachment->post_parent);

	if (is_a($post, 'WC_Product')) {

		// Not a product single
		if (!is_product()) {
			// Add class
			$attr['class'] .= ' woocommerce-loop-product__thumbnail';
		}
	}

	return $attr;
}
