<?php

/**
 * Change number of upsells output
 *
 * @link https://woocommerce.com/document/change-number-of-upsells-output/
 */

add_filter('woocommerce_upsell_display_args', 'assu_change_number_related_products', 20);
function assu_change_number_related_products($args)
{
	$args['posts_per_page'] = 3;
	$args['columns'] = 3;	//change number of upsells here

	return $args;
}
