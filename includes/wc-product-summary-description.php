<?php

/**
 * Exchange the excerpt for the full description in the product's singular summary
 */

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);

add_action('woocommerce_single_product_summary', 'assu_template_single_description', 20);
function assu_template_single_description()
{
	echo '<div class="description">' . get_the_content() . '</div>';
}
