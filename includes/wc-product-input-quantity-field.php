<?php

/**
 * Add two custom buttons of plus and minus for the quantity input
 */

add_action('woocommerce_before_quantity_input_field', 'assu_quantity_minus_sign');
function assu_quantity_minus_sign()
{
	echo sprintf(
		'<button type="button" class="%s" data-action="%s" title="%s" aria-label="%s" >%s</button>',
		'button',
		'minus',
		esc_html__('Minus', 'assu'),
		esc_html__('Reduce amount to add', 'assu'),
		'-'
	);
}

add_action('woocommerce_after_quantity_input_field', 'assu_quantity_plus_sign');
function assu_quantity_plus_sign()
{
	echo sprintf(
		'<button type="button" class="%s" data-action="%s" title="%s" aria-label="%s" >%s</button>',
		'button',
		'plus',
		esc_html__('Plus', 'assu'),
		esc_html__('Increase amount to add', 'assu'),
		'+'
	);
}
