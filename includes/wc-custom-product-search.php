<?php

/**
 * Custom Product Search Form
 *
 * @link https://woocommerce.com/document/overriding-the-product-search-box-widget/
 * @link https://hirejordansmith.com/how-to-modify-the-woocommerce-product-search-form/
 *
 */

add_filter('get_product_search_form', 'assu_custom_product_searchform');
function assu_custom_product_searchform($form)
{

	$label = sprintf(
		'<label class="screen-reader-text" for="s">%s</label>',
		esc_html__('Search for:', 'assu')
	);

	$searchField = sprintf(
		'<span class="search-field-wrapper">%s<input type="search" class="search-field" placeholder="%s" value="%s" name="s" title="%s" /></span>',
		file_get_contents(get_template_directory_uri() . '/assets/svg/search.svg'), // Icon
		esc_attr_x('Enter the cut of meat', 'placeholder', 'assu'), // Placeholder
		get_search_query(), // Value
		esc_attr_x('Search for:', 'label', 'assu') // Title
	);

	$buttonSubmit = sprintf(
		'<input type="submit" value="%s" />',
		esc_attr_x('Search', 'submit button', 'assu')
	);

	$hiddenInput = '<input type="hidden" name="post_type" value="product" />';

	$content = $label . $searchField . $buttonSubmit . $hiddenInput;

	$form = sprintf(
		'<form role="search" method="get" class="woocommerce-product-search" action="%s">%s</form>',
		esc_url(get_home_url()), // Form action
		$content
	);

	return $form;
}
