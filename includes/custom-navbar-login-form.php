<?php

/**
 * Custom navbar login form
 */

add_filter('wp_nav_menu_items', 'assu_main_menu_login', 10, 2);
function assu_main_menu_login($items, $args)
{
	// If this isn't the main menu, do nothing
	if (!($args->theme_location == 'main_menu'))
		return $items;

	if (is_user_logged_in()) {
		return $items . assu_main_menu_logged_user();
	} else {
		return $items . assu_main_menu_login_form();
	}
}

// Generet the menu item for a logged user
function assu_main_menu_logged_user()
{
	$currentUser = wp_get_current_user();
	$currentUserAvatar = get_avatar($currentUser->ID, 37, '', $currentUser->display_name);

	$htmlLoggedUser = '<li class="nav-item nav-account logged dropdown">';
	$htmlLoggedUser .= '<span class="nav-link dropdown-toggle">';
	$htmlLoggedUser .= $currentUserAvatar;
	$htmlLoggedUser .= $currentUser->display_name;
	$htmlLoggedUser .= '</span>';
	$htmlLoggedUser .= '<ul class="dropdown-menu" aria-label="' . esc_html__('Useful User Links', 'assu') . '">';
	if (class_exists('WooCommerce')) {
		$htmlLoggedUser .= '<li class="nav-item"><a href="' . esc_url(get_permalink(get_option('woocommerce_myaccount_page_id'))) . '" class="dropdown-item">' . esc_html__('My Account', 'assu') . '</a></li>';
	}
	$htmlLoggedUser .= '<li class="nav-item"><a href="' . esc_url(wp_logout_url(get_permalink())) . '" class="dropdown-item">' . esc_html__('Logout', 'assu') . '</a></li>';
	$htmlLoggedUser .= '</ul>';
	$htmlLoggedUser .= '</li>';

	return $htmlLoggedUser;
}

// Generet the menu item for a login form
function assu_main_menu_login_form()
{
	// Args for the wp login form function
	$wpLoginFormArgs = array(
		'echo' 				=> false,
		'label_username' 	=> esc_html__('Username or E-mail', 'assu'),
		'label_log_in'		=> esc_html__('Login', 'assu'),
	);

	$htmlLoginForm = '<li class="nav-item nav-account login dropdown">';
	$htmlLoginForm .= '<span class="nav-link dropdown-toggle">';
	$htmlLoginForm .= esc_html__('Log in', 'assu');
	$htmlLoginForm .= '</span>';
	$htmlLoginForm .= '<div class="dropdown-menu">';
	$htmlLoginForm .= wp_login_form($wpLoginFormArgs);
	if (class_exists('WooCommerce') && 'yes' === get_option('woocommerce_enable_myaccount_registration')) {
		$htmlLoginForm .= '<a class="register-link" href="' . esc_url(get_permalink(get_option('woocommerce_myaccount_page_id'))) . '">';
		$htmlLoginForm .= esc_html__('Create an account', 'assu');
		$htmlLoginForm .= '</a>';
	} else if (get_option('users_can_register')) {
		$htmlLoginForm .= '<a class="register-link" href="' . esc_url(wp_registration_url()) . '">';
		$htmlLoginForm .= esc_html__('Create an account', 'assu');
		$htmlLoginForm .= '</a>';
	}
	$htmlLoginForm .= '<a class="lostpassword-link" href="' . esc_url(wp_lostpassword_url()) . '">';
	$htmlLoginForm .= esc_html__('Lost your password?', 'assu');
	$htmlLoginForm .= '</a>';
	$htmlLoginForm .= '</div>';
	$htmlLoginForm .= '</li>';

	return $htmlLoginForm;
}
