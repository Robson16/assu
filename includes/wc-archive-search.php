<?php

/**
 * For add a product search box after the archive / shop page header
 */
add_action('woocommerce_before_shop_loop', 'assu_product_search_form', 15);
function assu_product_search_form()
{
	echo get_product_search_form();
}
