<?php

/**
 * Change number of related products output
 *
 * @link https://woocommerce.com/document/change-number-of-related-products-output/
 */

add_filter('woocommerce_output_related_products_args', 'assu_related_products_args', 20);
function assu_related_products_args($args)
{
	$args['posts_per_page'] = 3; 	// 3 related products
	$args['columns'] = 3; 			// arranged in 3 columns

	return $args;
}
