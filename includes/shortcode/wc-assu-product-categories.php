<?php

/**
 * Assu WooCommerce Product Categories List
 */

add_shortcode('assu_product_categories', 'assu_product_categories_shortcode');

function assu_product_categories_shortcode($atts)
{
	// Attributes
	$atts = shortcode_atts(
		array(
			'limit'			=> '6',
			'orderby'		=> 'name',
			'order'			=> 'ASC',
			'hide_empty'	=> 'true',
		),
		$atts
	);

	$product_categories = get_terms('product_cat', array(
		'number' 		=> $atts['limit'],
		'orderby'		=> $atts['orderby'],
		'order'      	=> $atts['order'],
		'hide_empty'	=> filter_var($atts['hide_empty'], FILTER_VALIDATE_BOOLEAN),
	));

	if (empty($product_categories)) {
		return;
	}

	$html = '';
	$html .= '<ul class="categories-list">';

	foreach ($product_categories as $product_category) {
		$thumbnail_id = get_term_meta($product_category->term_id, 'thumbnail_id', true);
		$thumbnail_image = wp_get_attachment_image($thumbnail_id, array('197', '193'));

		if (!$thumbnail_image) {
			$thumbnail_image = wc_placeholder_img();
		}

		$html .= '<li class="category-item">';
		$html .= '<a href="' . get_term_link($product_category) . '" >';
		$html .= $thumbnail_image;
		$html .= '<span>' . $product_category->name . '</span>';
		$html .= '</a>';
		$html .= '</li>';
	}

	$html .= '</ul>';

	return $html;
}
