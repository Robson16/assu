<?php

/**
 * Adds a quantity input field to each product on WooCommerce archives pages and other product loops.
 */

add_filter('woocommerce_loop_add_to_cart_link', 'assu_quantity_inputs_for_loop_ajax_add_to_cart', 10, 2);
function assu_quantity_inputs_for_loop_ajax_add_to_cart($html, $product)
{
	if (
		$product &&
		$product->is_type('simple') &&
		$product->is_purchasable() &&
		$product->is_in_stock() &&
		!$product->is_sold_individually()
	) {
		// Get the necessary classes
		$class = implode(' ', array_filter(array(
			'button',
			'product_type_' . $product->get_type(),
			$product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
			$product->supports('ajax_add_to_cart') ? 'ajax_add_to_cart' : '',
		)));

		if (is_user_logged_in()) {
			return sprintf(
				'<div class="woocommerce-loop-product__buy">%s<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">%s</a></div>',
				woocommerce_quantity_input(array(), $product, false), // Quantity field
				esc_url($product->add_to_cart_url()), // Url
				esc_attr(isset($quantity) ? $quantity : 1), // Quantity
				esc_attr($product->get_id()), // Product ID
				esc_attr($product->get_sku()), // Product SKU
				esc_attr(isset($class) ? $class : 'button'), // CSS Classes
				sprintf(
					'<i class="icon icon-cart">%s</i> %s',
					file_get_contents(get_template_directory_uri() . '/assets/svg/cart.svg'), // Icon
					esc_html($product->add_to_cart_text()), // Button Text
				)
			);
		} else {
			return sprintf(
				'<div class="woocommerce-loop-product__buy">%s<span class="%s disabled">%s</span></div>',
				woocommerce_quantity_input(array(), $product, false), // Quantity field
				esc_attr(isset($class) ? $class : 'button'), // CSS Classes
				sprintf(
					'<i class="icon icon-lock">%s</i> %s',
					file_get_contents(get_template_directory_uri() . '/assets/svg/lock.svg'), // Icon
					esc_html__('Login to buy', 'assu') // Button Text
				)
			);
		}
	}

	if (
		$product &&
		(!$product->is_type('simple') || $product->is_sold_individually())
	) {
		if (is_user_logged_in()) {
			return $html;
		} else {
			return sprintf(
				'<span class="%s disabled">%s</span>',
				esc_attr(isset($class) ? $class : 'button'), // CSS Classes
				sprintf(
					'<i class="icon icon-lock">%s</i> %s',
					file_get_contents(get_template_directory_uri() . '/assets/svg/lock.svg'), // Icon
					esc_html__('Login to buy', 'assu') // Button Text
				)
			);
		}
	}

	if (
		$product &&
		(!$product->is_purchasable() || !$product->is_in_stock())
	) {
		return sprintf(
			'<span class="unavailable-notice">%s</span>',
			esc_html__('Product Unavailable', 'assu'),
		);
	}
}
