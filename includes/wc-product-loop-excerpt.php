<?php

/**
 * Adds the excerpt of the product to the loop
 */
add_action('woocommerce_after_shop_loop_item_title', 'assu_template_loop_excerpt', 15);
function assu_template_loop_excerpt()
{
	echo sprintf(
		'<p class="woocommerce-loop-product__excerpt">%s</p>',
		(has_excerpt()) ? get_the_excerpt() : wp_trim_words(get_the_content(), 6, '[...]'),
	);
}
