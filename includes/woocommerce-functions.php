<?php

/**
 *
 * Functions to enhance and add features to the Woocommerce
 *
 */

/**
 * Enqueue styles and scripts for the theme
 */
add_action('wp_enqueue_scripts', 'assu_woocommerce_scripts');
function assu_woocommerce_scripts()
{
	// CSS
	wp_enqueue_style('assu-woocommerce-shared-styles', get_template_directory_uri() . '/assets/css/shared/woocommerce-shared-styles.css', array(), wp_get_theme()->get('Version'));
	wp_enqueue_style('assu-woocommerce-frontend-styles', get_template_directory_uri() . '/assets/css/frontend/woocommerce-frontend-styles.css', array(), wp_get_theme()->get('Version'));

	// JS
	wp_enqueue_script('assu-woocommerce-frontend-script', get_template_directory_uri() . '/assets/js/frontend/woocommerce-frontend-bundled.js', array(), wp_get_theme()->get('Version'), true);
}

/**
 * Register Woocommerce defaults and support for various WordPress features.
 */
add_action('after_setup_theme', 'assu_woocommerce_setup');
function assu_woocommerce_setup()
{
	// Enable basic support for WooCommerce
	add_theme_support('woocommerce');

	// Enable product gallery slider
	add_theme_support('wc-product-gallery-slider');

	// Load custom styles in the editor.
	add_editor_style(get_stylesheet_directory_uri() . '/assets/css/shared/woocommerce-shared-styles.css');
}

/**
 * Custom Actions and Filters
 */
require_once get_template_directory() . '/includes/wc-custom-product-search.php';
require_once get_template_directory() . '/includes/wc-archive-search.php';
require_once get_template_directory() . '/includes/wc-product-external-buy-button.php';
require_once get_template_directory() . '/includes/wc-product-loop-excerpt.php';
require_once get_template_directory() . '/includes/wc-product-loop-thumbnail.php';
require_once get_template_directory() . '/includes/wc-product-loop-add-to-cart.php';
require_once get_template_directory() . '/includes/wc-product-input-quantity-field.php';
require_once get_template_directory() . '/includes/wc-product-summary-description.php';
require_once get_template_directory() . '/includes/wc-cart-float-button.php';
require_once get_template_directory() . '/includes/wc-cart-item-description.php';
require_once get_template_directory() . '/includes/wc-upsell-products-args.php';
require_once get_template_directory() . '/includes/wc-related-products-args.php';
require_once get_template_directory() . '/includes/wc-catalog-orderby-options.php';

/**
 * Custom Shortcode
 */
require_once get_template_directory() . '/includes/shortcode/wc-assu-product-categories.php';
