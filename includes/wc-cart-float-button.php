<?php

/**
 * Cart Float Button
 *
 * @link https://woocommerce.com/document/show-cart-contents-total/
 */

add_action('wp_footer', 'assu_wc_cart_flag', 10);
function assu_wc_cart_flag()
{
	$content = sprintf(
		'<span class="cart-float-button__count">%d</span><i class="icon icon-cart">%s</i><span class="cart-float-button__total">%s</span><span class="cart-float-button__hover">%s</span>',
		WC()->cart->get_cart_contents_count(),
		file_get_contents(get_template_directory_uri() . '/assets/svg/cart.svg'), // Icon
		WC()->cart->get_cart_total(),
		file_get_contents(get_template_directory_uri() . '/assets/svg/chevron-circle-right.svg'), // Hover Icon
	);

	echo sprintf(
		'<div class="cart-float-button"><a href="%s" title="%s">%s</a></div>',
		esc_url(wc_get_cart_url()),
		esc_html__('View your shopping cart', 'assu'),
		$content
	);
}

// Show cart contents / total Ajax
add_filter('woocommerce_add_to_cart_fragments', 'assu_wc_cart_flag_fragment');
function assu_wc_cart_flag_fragment($fragments)
{
	global $woocommerce;

	$content = sprintf(
		'<span class="cart-float-button__count">%d</span><i class="icon icon-cart">%s</i><span class="cart-float-button__total">%s</span><span class="cart-float-button__hover">%s</span>',
		$woocommerce->cart->cart_contents_count,
		file_get_contents(get_template_directory_uri() . '/assets/svg/cart.svg'), // Icon
		$woocommerce->cart->get_cart_total(),
		file_get_contents(get_template_directory_uri() . '/assets/svg/chevron-circle-right.svg'), // Hover Icon
	);

	$button = sprintf(
		'<div class="cart-float-button"><a href="%s" title="%s">%s</a></div>',
		esc_url(wc_get_cart_url()),
		esc_html__('View your shopping cart', 'assu'),
		$content
	);

	$fragments['div.cart-float-button'] = $button;

	return $fragments;
}
