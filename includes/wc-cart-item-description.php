<?php

/**
 * Function to hook into the cart item
 *
 * Add the description text for the product after its name
 */

add_action('woocommerce_after_cart_item_name', 'assu_cart_item_description', 10, 2);
function assu_cart_item_description($cart_item, $cart_item_key)
{
	$product = $cart_item['data'];

	echo sprintf(
		"<p>%s</p>",
		($product->short_description)
			? $product->short_description
			: wp_trim_words($product->description, 6, '[...]'),
	);
}
