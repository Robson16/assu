<?php

/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.7.0
 */

defined('ABSPATH') || exit;
?>

<div class="woocommerce-order">

	<?php if ($order) : ?>

		<?php do_action('woocommerce_before_thankyou', $order->get_id());	?>

		<?php if ($order->has_status('failed')) : ?>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed">
				<?php esc_html_e('Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'assu'); ?>
			</p>

			<div class="woocommerce-order__card">

				<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
					<a href="<?php echo esc_url($order->get_checkout_payment_url()); ?>" class="button pay">
						<?php esc_html_e('Pay', 'assu'); ?>
					</a>

					<?php if (is_user_logged_in()) : ?>
						<a href="<?php echo esc_url(wc_get_page_permalink('myaccount')); ?>" class="button myaccount">
							<?php esc_html_e('My account', 'assu'); ?>
						</a>
					<?php endif; ?>
				</p>

				<?php do_action('woocommerce_thankyou', $order->get_id()); ?>

				<div class="woocommerce-order__payment-method">
					<?php do_action('woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id()); ?>
				</div>
				<!-- /.woocommerce-order__payment-method -->

			</div>
			<!-- /.woocommerce-order__card -->

		<?php else : ?>

			<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received">
				<?php
				echo apply_filters('woocommerce_thankyou_order_received_text', esc_html__('Thank you. Your order has been received.', 'assu'), $order); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
				?>
			</p>

			<div class="woocommerce-order__card">

				<span class="woocommerce-order-overview__date">
					<?php
					echo sprintf(
						esc_html__('%s Order %s created on %s', 'assu'),
						sprintf(
							'<i class="icon icon-check">%s</i>',
							file_get_contents(get_template_directory_uri() . '/assets/svg/check.svg'), // Icon
						),
						sprintf('<strong>#%s</strong>',	$order->get_order_number()), // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
						wc_format_datetime($order->get_date_created()), // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
					);
					?>
				</span>

				<span class="woocommerce-order-overview__total">
					<?php
					echo sprintf(
						esc_html__('Total: %s', 'assu'),
						$order->get_formatted_order_total(), // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
					);
					?>
				</span>

				<?php if (is_user_logged_in() && $order->get_user_id() === get_current_user_id()) : ?>
					<span class="woocommerce-order-overview__billing-address">
						<?php
						echo sprintf(
							esc_html('%s | %s - %s'),
							sprintf('<strong>%s</strong>',	$order->get_billing_first_name()), // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
							$order->get_billing_address_1(), // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
							$order->get_billing_address_2(), // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
						);
						?>
					</span>
				<?php endif; ?>

				<?php do_action('woocommerce_thankyou', $order->get_id()); ?>

				<div class="woocommerce-order__payment-method">
					<?php do_action('woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id()); ?>
				</div>
				<!-- /.woocommerce-order__payment-method -->

			</div>
			<!-- /.woocommerce-order__card -->

		<?php endif; ?>

	<?php else : ?>

		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received">
			<?php
			echo apply_filters('woocommerce_thankyou_order_received_text', esc_html__('Thank you. Your order has been received.', 'assu'), null); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			?>
		</p>

	<?php endif; ?>

</div>
