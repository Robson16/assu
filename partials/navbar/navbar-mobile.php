<?php

/**
 * Fixed navigation bar on footer for mobile devices
 */

?>

<nav id="navbar-mobile" class="navbar navbar-mobile">

	<ul class="navbar-togglers-list">
		<?php
		// My account
		if (class_exists('WooCommerce')) {
			echo sprintf(
				'<li><a href="%s" aria-label="%s" title="%s"><i class="icon icon-account">%s</i></a></li>',
				esc_url(get_permalink(get_option('woocommerce_myaccount_page_id'))), // Link to my account page
				esc_html__('Go to my account', 'assu'), // Aria label
				esc_html__('Go to my account', 'assu'), // Title
				file_get_contents(get_template_directory_uri() . '/assets/svg/account.svg'), // Icon
			);
		}

		// Menu toggler
		echo sprintf(
			'<li><button type="button" class="navbar-toggler" aria-label="%s" title="%s" data-target="%s"><i class="icon icon-menu">%s</i></button></li>',
			esc_html__('Open navigation menu', 'assu'), // Aria label
			esc_html__('Open navigation menu', 'assu'), // Title
			'#navbar-mobile-nav', // Target
			file_get_contents(get_template_directory_uri() . '/assets/svg/menu.svg'), // Icon
		);

		// Search toggler
		echo sprintf(
			'<li><button type="button" class="navbar-toggler" aria-label="%s" title="%s" data-target="%s"><i class="icon icon-search">%s</i></button></li>',
			esc_html__('Open search form', 'assu'), // Aria label
			esc_html__('Open search form', 'assu'), // Title
			'#navbar-mobile-search', // Target
			file_get_contents(get_template_directory_uri() . '/assets/svg/search.svg'), // Icon
		);
		?>
	</ul>
	<!-- /.navbar-togglers-list -->


	<?php
	wp_nav_menu(array(
		'theme_location' => 'mobile_menu',
		'depth' => 2,
		'container' => 'div',
		'container_class' => 'collapse navbar-collapse',
		'container_id' => 'navbar-mobile-nav',
		'menu_class' => 'navbar-nav',
		'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
		'walker' => new WP_Bootstrap_Navwalker()
	));
	?>

	<div id="navbar-mobile-search" class="collapse navbar-collapse search">
		<?php
		if (class_exists('WooCommerce')) : ?>
			<p>
				<?php esc_html_e('Search for:', 'assu'); ?>
			</p>
			<?php get_product_search_form(); ?>
		<?php else : ?>
			<?php get_search_form(); ?>
		<?php endif; ?>
	</div>
</nav>
<!-- /.navbar-mobile -->
