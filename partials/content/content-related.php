<?php

/**
 * The template for displaying related posts
 */
?>

<div class="related">
	<?php
	$tags = wp_get_post_tags($post->ID);
	$tags_id = array_column($tags, 'term_id');

	if ($tags) :
	?>
		<h4 class="related__title"> <?php _e('Related Posts', 'assu'); ?> </h4>
		<div class="related__grid">
			<?php
			$my_query = new WP_Query(array(
				'tag__in'				=> $tags_id,
				'post__not_in'			=> [$post->ID],
				'posts_per_page'		=> 3,
				'ignore_sticky_posts'	=> true,
				'orderby'				=> 'rand',
			));

			if ($my_query->have_posts()) {
				while ($my_query->have_posts()) {
					$my_query->the_post();

					echo sprintf('<a class="related-post" href="%s" title="%s">', esc_url(get_permalink()), get_the_title());
					the_post_thumbnail('thumbnail_related', array('title' => get_the_title()));
					the_title('<h3 class="related-post__title">', '</h3>');
					echo sprintf('<p class="related-post__meta">%s</p>', date_i18n(get_option('date_format'), get_post_timestamp()));
					echo '</a>';
				}
				wp_reset_query();
			}
			?>
		</div>
		<!-- /.related-grid -->
	<?php endif; ?>
</div>
<!-- /.related-posts -->