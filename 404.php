<?php

/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 */
get_header();
?>

<main>
	<section class="container">
		<div class="error-message">
			<span>404</span>
			<h1 class="page-title"><?php _e("Page not found", 'assu'); ?></h1>
			<p><?php _e('Could not find the page you are looking for.', 'assu'); ?></p>
			<a href="<?php echo get_home_url(); ?>"><?php _e('Return to home page', 'assu'); ?></a>
		</div>
	</section>
</main>

<?php
get_footer();
