<?php

/**
 * The template for displaying the footer
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */

?>

<footer class="footer">
	<div class="footer-widgets">
		<div class="container">
			<?php if (is_active_sidebar('assu-sidebar-footer-1')) : ?>
				<div class="widget-column">
					<?php dynamic_sidebar('assu-sidebar-footer-1'); ?>
				</div>
			<?php endif; ?>
			<?php if (is_active_sidebar('assu-sidebar-footer-2')) : ?>
				<div class="widget-column">
					<?php dynamic_sidebar('assu-sidebar-footer-2'); ?>
				</div>
			<?php endif; ?>
			<?php if (is_active_sidebar('assu-sidebar-footer-3')) : ?>
				<div class="widget-column">
					<?php dynamic_sidebar('assu-sidebar-footer-3'); ?>
				</div>
			<?php endif; ?>
			<?php if (is_active_sidebar('assu-sidebar-footer-4')) : ?>
				<div class="widget-column">
					<?php dynamic_sidebar('assu-sidebar-footer-4'); ?>
				</div>
			<?php endif; ?>
			<?php if (is_active_sidebar('assu-sidebar-footer-5')) : ?>
				<div class="widget-column">
					<?php dynamic_sidebar('assu-sidebar-footer-5'); ?>
				</div>
			<?php endif; ?>
		</div>
		<!-- /.container -->
	</div>
	<!-- /.footer-widgets -->

	<div class="footer-copyright">
		<div class="container">
			<img src="<?php echo get_site_icon_url(40, 'https://via.placeholder.com/40'); ?>" width="40" height="40" alt="<?php echo bloginfo('title'); ?>">
			<span>&copy;&nbsp;<?php echo bloginfo('title'); ?>&nbsp;<?php echo wp_date('Y'); ?>&nbsp;&#8212;&nbsp;<?php _e('All rights reserved.', 'assu') ?></span>
			<span>
				<?php _e('Developed by', 'assu') ?>
				&nbsp;
				<a class="developer-brand" href="https://agenciabeb.com.br/" target="_blank" rel="noopener">
					<img src="<?php echo get_template_directory_uri() . '/assets/images/agenciabeb.png'; ?>" alt="Agência B&B">
				</a>
			</span>
		</div>
		<!-- /.container -->
	</div>
	<!-- /.footer-copyright -->
</footer>

<?php wp_footer(); ?>

</body>

</html>
